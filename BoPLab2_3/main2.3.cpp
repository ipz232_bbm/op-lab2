#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main() {
	int a, b, c, d;

	printf("a = ");
	scanf("%d", &a);
	printf("b = ");
	scanf("%d", &b);
	printf("c = ");
	scanf("%d", &c);
	printf("d = ");
	scanf("%d", &d);

	a = a + d; printf("\na = %d\n", a);
	d = a - d; printf("d = %d\n", d);
	a = a - d; printf("a = %d\n", a);

	c = c + b; printf("\nc = %d\n", c);
	b = c - b; printf("b = %d\n", b);
	c = c - b; printf("c = %d\n", c);
								  
	d = d + c; printf("\nd = %d\n", d);
	c = d - c; printf("c = %d\n", c);
	d = d - c; printf("d = %d\n\n", d);

	printf("a = %d\n", a);
	printf("b = %d\n", b);
	printf("c = %d\n", c);
	printf("d = %d\n", d);

	return 0;
}