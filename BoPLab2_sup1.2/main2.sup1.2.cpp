#define _USE_MATH_DEFINES
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>

int main() {
	double a, b, c, h, alpha, beta, gamma, S, P;

	printf("a = ");
	scanf("%lf", &a);
	printf("h = ");
	scanf("%lf", &h);
	printf("gamma = (pi) ");
	scanf("%lf", &gamma);
	gamma *= M_PI;

	beta = asin(h / a);
	printf("\nbeta = %f pi", beta / M_PI);
	alpha = M_PI - gamma - beta;
	printf("\nalpha = %f pi", alpha / M_PI);
	b = h / sin(alpha);
	printf("\n\nb = %f", b);
	c = sqrt(a * a + b * b - 2 * a * b * cos(gamma));
	printf("\nc = %f", c);

	P = a + b + c;
	printf("\n\nP = %f", P);
	S = h * c / 2;
	printf("\nS = %f", S);

	return 0;
}