#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <windows.h>
#include <Math.h>

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	double alpha_deg, alpha_rad;
	scanf("%lf", &alpha_deg);

	if (alpha_deg > 0 && alpha_deg < 360) {
		alpha_rad = alpha_deg / 180 * M_PI;
		printf("%f", alpha_rad);
	}
	else printf("���� �������� �� ������� ����.");

	return 0;
}