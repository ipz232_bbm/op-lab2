#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>

int main() {

	double x, y, z, s;
	printf("x = ");
	scanf("%lf", &x);
	printf("y = ");
	scanf("%lf", &y);
	printf("z = ");
	scanf("%lf", &z);

	s = pow((cos(x) - sin(y)), 1 + 2 * sin(z));
	s *= z + z * z / 2 + z * z * z / 3 + z * z * z * z / 4;
	printf("%lf", s);

	return 0;
}
